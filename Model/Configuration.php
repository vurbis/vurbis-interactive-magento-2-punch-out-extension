<?php

namespace Vurbis\Punchout\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Configuration Model
 */
class Configuration
{
    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;
    /**
     * @var WriterInterface
     */
    protected $configWriter;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * Constructor
     *
     * @param ScopeConfigInterface $scopeConfig
     * @param WriterInterface $configWriter
     * @param StoreManagerInterface $storeManage
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        WriterInterface $configWriter,
        StoreManagerInterface $storeManager
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->configWriter = $configWriter;
        $this->storeManager = $storeManager;
    }
    /**
     * Get API Url ('api_url')
     *
     * @retun string
     */
    public function getApiUrl()
    {
        $url = $this->__getApiSettings('api_url');
        if (!$url) {
            throw new LocalizedException(__('API URL is not configured.'));
        }
        return $url;
    }

    /**
     * Get Supplier ID ('supplier_id')
     *
     * @return string
     */
    public function getSupplierId()
    {
        $supplier_id = $this->__getApiSettings('supplier_id');
        if (!$supplier_id) {
            throw new LocalizedException(__('Supplier ID is not configured.'));
        }
        return $supplier_id;
    }
    
    /**
     * Is Punchout plugin enabled ('punchout_enabled')
     *
     * @return bool
     */
    public function isPluginEnabled()
    {
        $enabled = (int)$this->__getApiSettings('punchout_enabled');
        return (bool)$enabled;
    }

    /**
     * Is Cron enabled ('cron_enabled')
     *
     * @return bool
     */
    public function isCronEnabled()
    {
        $enabled = (int)$this->__getCronSettings('cron_enabled');
        return (bool)$enabled;
    }

    /**
     * Get number of days set for the cron configuration ('cron_deletion_days') 
     *
     * @return string
     */
    public function getCronDays()
    {
        $cron_days = $this->__getCronSettings('cron_deletion_days');
        if (!$cron_days) {
            throw new LocalizedException(__('Cron deletion days is not configured.'));
        }
        return $cron_days;
    }
    
    /**
     * Get api setting value
     *
     * @param string $name
     * @return value
     */
    private function __getApiSettings($name)
    {
        $value = $this->scopeConfig->getValue(
            'vurbis_punchout/api/' . $name,
            ScopeInterface::SCOPE_WEBSITE
        );
        // Trimming on null breaks
        if ($value !== null) {
            $value = trim($value);
        }
        return $value;
    }

    /**
     * Get cron setting value
     *
     * @param string $name
     * @return value
     */
    private function __getCronSettings($name)
    {
        $value = $this->scopeConfig->getValue(
            'vurbis_punchout/cron_settings/' . $name,
            ScopeInterface::SCOPE_WEBSITE
        );
        if ($value !== null) {
            $value = trim($value);
        }
        return $value;
    }


    /**
     * Set punchout configuration 
     *
     * @param array $configData
     * @return void
     */
    public function setConfig(array $configData)
    {
        // Default values for the allowed properties
        $defaultAllowedConfig = [
            'api_url' => 'Add-Vurbis-Api-URL',
            'supplier_id' => 'Add-Vurbis-Supplier_id',
            'punchout_enabled' => '1',
            'cron_enabled' => '0',
            'cron_deletion_days' => '14'
        ];

        // Only allow the allowed properties to be added
        $filteredConfig = array_intersect_key($configData, $defaultAllowedConfig);
        $finalConfig = array_merge($defaultAllowedConfig, $filteredConfig);

        foreach ($finalConfig as $key => $value) {
            if ($key === 'punchout_enabled' || $key === 'cron_enabled') {
                $value = filter_var($value, FILTER_VALIDATE_BOOLEAN) ? '1' : '0';
            }

            // Params: (path, value, scope, scope_id)
            $this->configWriter->save(
                'vurbis_punchout/' . (strpos($key, 'cron_') === 0 ? 'cron_settings/' : 'api/') . $key,
                $value,
                ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
                0
            );
        }

    }

}
