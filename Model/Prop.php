<?php

namespace Vurbis\Punchout\Model;

class Prop
{

    /**
     * @var string
     */
    public $kind;

    /**
     * @var string
     */
    public $table;

    /**
     * @var string
     */
    public $field;

    /**
     * @var string
     */
    public $value;

    /**
     * Gets kind.
     *
     * @api
     * @return string
     */
    public function getKind()
    {
        return $this->kind;
    }

    /**
     * Sets kind.
     *
     * @api
     * @param string $kind
     * @return void
     */
    public function setKind($kind)
    {
        $this->kind = $kind;
    }

    /**
     * Gets table.
     *
     * @api
     * @return string
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * Sets table.
     *
     * @api
     * @param string $table
     * @return void
     */
    public function setTable($table)
    {
        $this->table = $table;
    }

    /**
     * Gets field.
     *
     * @api
     * @return string
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * Sets field.
     *
     * @api
     * @param string $field
     * @return void
     */
    public function setField($field)
    {
        $this->field = $field;
    }

    /**
     * Gets value.
     *
     * @api
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Sets value.
     *
     * @api
     * @param string $value
     * @return void
     */
    public function setValue($value)
    {
        $this->value = $value;
    }
}
