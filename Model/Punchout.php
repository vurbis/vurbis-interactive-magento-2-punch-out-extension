<?php

namespace Vurbis\Punchout\Model;

/**
 * Punchout Model
 */
class Punchout
{    
    /**
     * Send curl request
     *
     * @param string $url
     * @param array $data
     * @param string $format json|xml
     * @param string $response
     * @return object
     * @throws \Magento\Framework\Exception\LocalizedException
     */    
    public function post($url, $data = null, $format = 'json', $response = 'json')
    {
        $headers = [
            'Accept: application/' . $response,
            'Content-Type: application/' . $format,
        ];
        $handle = curl_init();
        curl_setopt($handle, CURLOPT_URL, $url);
        curl_setopt($handle, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handle, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($handle, CURLOPT_POST, true);
        if ($format == 'json' && isset($data)) {
            $data = json_encode($data);
        }
        curl_setopt($handle, CURLOPT_POSTFIELDS, $data);
        if ($response == 'json') {
            $response = json_decode(curl_exec($handle));
        } else {
            $response = curl_exec($handle);
        }
        curl_close($handle);
        if (isset($response->error) && isset($response->message)) {
            throw new \Magento\Framework\Exception\LocalizedException(__($response->message));
        }
        return $response;
    }     
    
    public function get($url)
    {
        $handle = curl_init();
        curl_setopt($handle, CURLOPT_URL, $url);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handle, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($handle);
        curl_close($handle);
        return $response;
    }     
}
