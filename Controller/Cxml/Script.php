<?php

namespace Vurbis\Punchout\Controller\Cxml;

use Magento\Framework\App\Action\Action;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\UrlInterface;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Customer\Model\Session as CustomerSession;
use Vurbis\Punchout\Model\Configuration;
use Vurbis\Punchout\Model\Punchout;

/**
 * Script controller
 */
class Script extends Action
{
    /**
     * @var Configuration
     */
    protected $configuration;

    /**
     * @var Punchout
     */
    protected $punchout;
    
    /**
     * @var UrlInterface
     */
    protected $urlInterface;

    /**
     * @var CustomerSession
     */
    protected $session;
    
    /**
     * @var CheckoutSession
     */
    protected $checkoutSession;

    /**
     * @var ObjectManagerInterface
     */
    protected $_object;

    /**
     *
     * @param Context $context
     * @param Configuration $configuration
     * @param Punchout $punchout
     * @param UrlInterface $urlInterface
     * @param CustomerSession $session
     * @param CheckoutSession $checkoutSession
     */
    public function __construct(
        Context $context,
        Configuration $configuration,
        Punchout $punchout,
        UrlInterface $urlInterface,
        CustomerSession $session,
        CheckoutSession $checkoutSession
    ) {
        parent::__construct($context);
        $this->configuration = $configuration;
        $this->punchout = $punchout;
        $this->urlInterface = $urlInterface;
        $this->session = $session;
        $this->checkoutSession = $checkoutSession;
    }
    
    /**
     * Add script tag to header
     */
    public function execute()
    {
        $sessionId = $this->session->getPunchoutSession();
        if (empty($sessionId)) {
            $response = "function initPunchout(){}//" . time();
        } else {
            $apiUrl = $this->configuration->getApiUrl();
            $cart = $this->checkoutSession->getQuote();
            $url = $apiUrl . '/punchout/files/' . $sessionId . '/magento2.js?proxy=true&cart=' . $cart->getId();
            $response = $this->punchout->get($url);
        }
        $result = $this->resultFactory->create(ResultFactory::TYPE_RAW);
        return $result
            ->setHeader('Cache-Control', 'no-store, no-cache, must-revalidate, max-age=0', true)
            ->setHeader('Content-Type', 'application/javascript;charset=UTF-8')
            ->setContents($response);
    }
}
