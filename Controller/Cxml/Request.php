<?php

namespace Vurbis\Punchout\Controller\Cxml;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\UrlInterface;
use Magento\Framework\Filesystem\Driver\File;
use Magento\Framework\Controller\ResultFactory;
use Vurbis\Punchout\Model\Configuration;
use Vurbis\Punchout\Model\Punchout;

/**
 * Request controller
 */
class Request extends Action
{
    /**
     * @var Configuration
     */
    protected $configuration;
    /**
     * @var Punchout
     */
    protected $punchout;
    /**
     * @var UrlInterface
     */
    protected $urlInterface;
    /**
     * @var File
     */
    protected $fileSystem;

    /**
     * @param Context $context
     * @param Configuration $configuration
     * @param Punchout $punchout
     * @param UrlInterface $urlInterface
     * @param File $fileSystem
     */
    public function __construct(
        Context $context,
        Configuration $configuration,
        Punchout $punchout,
        UrlInterface $urlInterface,
        File $fileSystem
    ) {
        parent::__construct($context);
        $this->configuration = $configuration;
        $this->punchout = $punchout;
        $this->urlInterface = $urlInterface;
        $this->fileSystem = $fileSystem;
    }
    /**
     * Request action
     */
    public function execute()
    {
        $apiUrl = $this->configuration->getApiUrl();
        $url = $this->urlInterface->getCurrentUrl();
        $path = explode("/punchout/cxml", $url)[1];
        $path = str_replace("/setup", "/request", $path);
        $url = $apiUrl . '/punchout' . $path;
        $body = $this->fileSystem->fileGetContents('php://input');
        $req = $this->getRequest();
        $response = $this->punchout->post(
            $url, 
            [
                'cxml' => $body,
                'query' => $req->getParams(),
                'servers' => $req->getServer() // includes headers -> HTTP_ properties
            ], 
            "json", 
            "xml"
        );
        $result = $this->resultFactory->create(ResultFactory::TYPE_RAW);
        return $result->setHeader('Content-Type', 'text/xml')->setContents($response);
    }
}
