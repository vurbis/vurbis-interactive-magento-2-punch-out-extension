<?php

namespace Vurbis\Punchout\Api;

/**
 * ApiProductsResponseInterface Api
 */
interface ApiProductsResponseInterface extends \Magento\Framework\Api\CustomAttributesDataInterface
{
    /**#@+
     * Constants defined for keys of the data array. Identical to the name of the getter in snake case
     */
    public const SKU = 'sku';

    public const PARENT_SKUS = 'parentSkus';

    public const KIND = 'kind';

    /**#@-*/

    /**
     * Get Sku.
     *
     * @return string|null
     */
    public function getSku();

    /**
     * Set Sku.
     *
     * @param string $sku
     * @return $this
     */
    public function setSku($sku);

    /**
     * Get kind.
     *
     * @return string|null
     */
    public function getKind();

    /**
     * Set kind.
     *
     * @param string $kind
     * @return $this
     */
    public function setKind($kind);
    /**
     * Get Parent Skus.
     *
     * @return string|null
     */
    public function getParentSkus();

    /**
     * Set Parent Skus.
     *
     * @param string $parentSkus
     * @return $this
     */
    public function setParentSkus($parentSkus);
}
